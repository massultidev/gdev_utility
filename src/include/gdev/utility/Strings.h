/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_UTILITY_STRINGS_H_
#define SRC_INCLUDE_GDEV_UTILITY_STRINGS_H_

#include <Poco/SharedPtr.h>

#include <string>
#include <vector>
#include <map>

#define _fixStrWX( text ) wxString::FromUTF8( ( text ).c_str(), ( text ).size() )
#define _getStrWX( label ) wxString::FromUTF8( gdev::utility::Strings::getInstance()->getString( label ).c_str(), gdev::utility::Strings::getInstance()->getString( label ).size() )
#define _fixStrSTL( wxText ) std::string( ( wxText ).utf8_str().data(), ( wxText ).utf8_str().length() )

namespace gdev {
namespace utility {

class Strings
{
	public:
		typedef Poco::SharedPtr < Strings > Ptr;

		virtual ~Strings();
		static Strings::Ptr getInstance();

		std::string getString( const std::string& label ) const;
		std::vector < std::string > getLangs() const;
		std::string getLang() const;
		bool setLang( const std::string& lang );

	private:
		Strings();
		void init();

		static Strings::Ptr mInstance;
		mutable std::map < std::string, std::map < std::string, std::string > > mStrings;
		std::string mCurLang;
};

}
}

#endif
