/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_UTILITY_RESOURCES_H_
#define SRC_INCLUDE_GDEV_UTILITY_RESOURCES_H_

#include <Poco/SharedPtr.h>
#include <Poco/Zip/ZipArchive.h>

#include <string>

#define _getBitmapWX( name ) \
	wxBitmapHelpers::NewFromPNGData( \
			gdev::utility::Resources::getInstance()->get( name ).c_str(), \
			gdev::utility::Resources::getInstance()->get( name ).size() \
	)

namespace gdev {
namespace utility {

class Resources
{
	public:
		typedef Poco::SharedPtr < Resources > Ptr;

		virtual ~Resources();
		static Resources::Ptr getInstance();

		bool has( const std::string& resPath ) const;
		std::string get( const std::string& resPath ) const;

	private:
		Resources();

		static Resources::Ptr mInstance;
		Poco::SharedPtr < Poco::Zip::ZipArchive > mZip;
		mutable std::string mContent;
};

}
}

#endif
