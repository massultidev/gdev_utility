/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_UTILITY_LOGGABLE_H_
#define SRC_INCLUDE_GDEV_UTILITY_LOGGABLE_H_

#include <Poco/Logger.h>
#include <Poco/Message.h>

#include <string>

namespace gdev {
namespace utility {

class Loggable
{
	public:
		virtual ~Loggable();

	protected:
		Loggable( const std::string& name );

		static void init(
				Poco::Message::Priority priority=Poco::Message::PRIO_DEBUG,
				const std::string& path="",
				const std::string& opt="10 M | number | utc | true | 2"
		);

		Poco::Logger& log() const;

	private:
		static std::size_t mLoggablesCounter;
		static std::string mOutputFormat;

		std::string mName;
};

}
}

#endif
