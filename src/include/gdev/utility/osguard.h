/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_UTILITY_OSGUARD_H
#define SRC_INCLUDE_GDEV_UTILITY_OSGUARD_H

#define _IS_LINUX \
	defined(linux) || \
	defined(__linux) || \
	defined(__linux__) || \
	defined(LINUX) || \
	defined(__LINUX) || \
	defined(__LINUX__) || \
	defined(unix) || \
	defined(__unix) || \
	defined(__unix__) || \
	defined(__UNIX) || \
	defined(__UNIX__)

#define _IS_WINDOWS \
	defined(WIN32) || \
	defined(_WIN32) || \
	defined(__WIN32) || \
	defined(__WIN32__) || \
	defined(WIN64) || \
	defined(_WIN64) || \
	defined(__WIN64) || \
	defined(__WIN64__) || \
	defined(__MINGW__) || \
	defined(__MINGW32__) || \
	defined(__MINGW64__) || \
	defined(__CYGWIN__) || \
	defined(__CYGWIN32__) || \
	defined(__CYGWIN64__)

namespace gdev {

inline std::string getBuildOS() {
	#if _IS_LINUX
		return "Linux";
	#elif _IS_WINDOWS
		return "Windows";
	#else
		return "Unknown";
	#endif
}

}

#endif

