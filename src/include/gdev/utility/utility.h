/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_UTILITY_UTILITY_H_
#define SRC_INCLUDE_GDEV_UTILITY_UTILITY_H_

#include <Poco/Dynamic/Var.h>

#define IGNORE_REF( ref ) gdev::utility::ignoreRef( ref )
#define IGNORE_PTR( ptr ) gdev::utility::ignorePtr( ptr )

// Guard that template type argument is derived from desired base type:
#define TEMPLATE_INHERITANCE_GUARD( TypeDerived, TypeBase ) { \
		TypeDerived* typeDerivedInstance = NULL; \
		TypeBase* typeBaseInstance = typeDerivedInstance; \
		IGNORE_PTR( typeBaseInstance ); \
	}

namespace gdev {
namespace utility {

void ignoreRef(const Poco::Dynamic::Var&);

void ignorePtr(const void*);

}
}

#endif
