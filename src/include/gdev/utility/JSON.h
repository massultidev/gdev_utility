/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_UTILITY_JSONPARSER_H_
#define SRC_INCLUDE_GDEV_UTILITY_JSONPARSER_H_

#include <Poco/Dynamic/Var.h>

#include <string>

namespace gdev {
namespace utility {

class JSON
{
	public:
		static Poco::Dynamic::Var::Ptr toJSON(const std::string& str);
		static std::string toString(const Poco::Dynamic::Var& dVar, bool compressed=true);

		static bool isJSON(const std::string& str);
		static bool isJSON(const Poco::Dynamic::Var& dVar);

	private:
		JSON();
};

}
}

#endif
