/* Copyright by: P.J. Grochowski */

#include "gdev/utility/Resources.h"

#include <Poco/Zip/ZipStream.h>
#include <Poco/StreamCopier.h>

#include <sstream>

// Build time provided variables:
#if defined(__GNUC__) || defined(__MINGW32__)
	char __RESOURCE_GUARD__ = 0;
	extern char _binary_gdev_resource_zip_start;
	extern char _binary_gdev_resource_zip_end;
	#pragma weak _binary_gdev_resource_zip_start 	= __RESOURCE_GUARD__
	#pragma weak _binary_gdev_resource_zip_end 		= __RESOURCE_GUARD__
#else
	#error "This project only supports GNU GCC like compilers!"
#endif

namespace gdev {
namespace utility {

Resources::Ptr Resources::mInstance = NULL;

Resources::Resources() {
	if(
			( &::_binary_gdev_resource_zip_start == &::__RESOURCE_GUARD__ )
		||
			( &::_binary_gdev_resource_zip_end == &::__RESOURCE_GUARD__ )
	)
	{
		throw Poco::Exception( "No binary resource embedded!" );
	}

	mContent = std::string(
			&::_binary_gdev_resource_zip_start,
			&::_binary_gdev_resource_zip_end
	);
	std::stringstream sstr( mContent );
	std::istream istr( sstr.rdbuf() );
	mZip = new Poco::Zip::ZipArchive( istr );
}
Resources::~Resources() {
}
Resources::Ptr Resources::getInstance() {
	if( mInstance.isNull() ) {
		mInstance = new Resources();
	}
	return mInstance;
}

bool Resources::has( const std::string& resPath ) const {
	Poco::Zip::ZipArchive::FileHeaders::const_iterator it = mZip->findHeader( resPath );
	if( it != mZip->headerEnd() ) {
		return true;
	}
	return false;
}

std::string Resources::get( const std::string& resPath ) const {
	Poco::Zip::ZipArchive::FileHeaders::const_iterator it = mZip->findHeader( resPath );
	if( it == mZip->headerEnd() ) {
		throw Poco::Exception( "Resource: '%s' - Not found!", resPath );
	}

	std::stringstream sstr( mContent );
	std::istream istr( sstr.rdbuf() );
	Poco::Zip::ZipInputStream zistr( istr, it->second );
	std::stringstream sstr2;
	Poco::StreamCopier::copyStream( zistr, sstr2 );

	return sstr2.str();
}

}
}
