/* Copyright by: P.J. Grochowski */

#include "gdev/utility/utility.h"

namespace gdev {
namespace utility {

void ignoreRef( const Poco::Dynamic::Var& ) {
}

void ignorePtr( const void * ) {
}

}
}

