/* Copyright by: P.J. Grochowski */

#include "gdev/utility/JSON.h"

#include <Poco/Exception.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>
#include <Poco/JSON/Parser.h>
#include <Poco/JSON/Stringifier.h>

namespace gdev {
namespace utility {

JSON::JSON() {
}

Poco::Dynamic::Var::Ptr JSON::toJSON( const std::string& str ) {
	Poco::Dynamic::Var dVar;
	Poco::JSON::Parser jsonParser;
	try {
		dVar = jsonParser.parse( str );
		dVar = jsonParser.result();
		Poco::Dynamic::Var::Ptr cVPtr = new Poco::Dynamic::Var();
		*cVPtr = dVar;
		return cVPtr;
	}
	catch( const Poco::Exception& e ) {
	}
	catch( const std::exception& e ) {
	}
	catch( ... ) {
	}
	throw Poco::Exception( "Not a JSON or syntax error!" );
}

std::string JSON::toString( const Poco::Dynamic::Var& dVar, bool compressed ) {
	if( !isJSON( dVar ) ) {
		throw Poco::Exception( "Not a JSON!" );
	}
	std::stringstream sstr;
	sstr.str( std::string() );
	Poco::JSON::Stringifier::stringify( dVar, sstr, (int)(!compressed), 4 );
	return sstr.str();
}

bool JSON::isJSON( const std::string& str ) {
	Poco::Dynamic::Var dVar;
	Poco::JSON::Parser jsonParser;
	try {
		dVar = jsonParser.parse( str );
		dVar = jsonParser.result();
		return true;
	}
	catch( const Poco::Exception& e ) {
	}
	catch( const std::exception& e ) {
	}
	catch( ... ) {
	}
	return false;
}

bool JSON::isJSON( const Poco::Dynamic::Var& dVar ) {
	return (
			dVar.type() == typeid( Poco::JSON::Object::Ptr )
		||
			dVar.type() == typeid( Poco::JSON::Array::Ptr )
		||
			dVar.type() == typeid( Poco::JSON::Object )
		||
			dVar.type() == typeid( Poco::JSON::Array )
	);
}

}
}
