/* Copyright by: P.J. Grochowski */

#include "gdev/utility/Strings.h"

#include "gdev/utility/Resources.h"
#include "gdev/utility/JSON.h"

#include <Poco/Exception.h>
#include <Poco/Format.h>
#include <Poco/JSON/Object.h>
#include <Poco/JSON/Array.h>

#include <sstream>
#include <limits>

namespace gdev {
namespace utility {

Strings::Ptr Strings::mInstance = NULL;

Strings::Strings() {
}
Strings::~Strings() {
}
Strings::Ptr Strings::getInstance() {
	if( mInstance.isNull() ) {
		mInstance = new Strings();
		mInstance->init();
	}

	return mInstance;
}

std::string Strings::getString( const std::string& label ) const {
	if( !mStrings.count( mCurLang ) ) {
		return "<missing-language>";
	}
	if( !mStrings[ mCurLang ].count( label ) ) {
		return "<missing-string>";
	}
	
	std::string str = ( mStrings[ mCurLang ] )[ label ];
	if( str.empty() ) {
		return "<missing-string>";
	}
	return str;
}

std::vector < std::string > Strings::getLangs() const {
	std::vector < std::string > langs;
	for( std::map < std::string, std::map < std::string, std::string > >::iterator
			it = mStrings.begin(), itEnd = mStrings.end(); it != itEnd; it++ )
	{
		langs.push_back( it->first );
	}

	return langs;
}

std::string Strings::getLang() const {
	return mCurLang;
}

bool Strings::setLang( const std::string& lang ) {
	if( !mStrings.count( lang ) ) {
		return false;
	}
	
	mCurLang = lang;
	return true;
}

void Strings::init() {
	Resources::Ptr rPtr = Resources::getInstance();
	std::string str = rPtr->get( "strings.json" );
	Poco::JSON::Object::Ptr oPtr = JSON::toJSON( str )->extract < Poco::JSON::Object::Ptr >();

    const std::string LANGUAGES = "languages";
    const std::string LABELS = "labels";
    if( ( oPtr->size() != 2 ) || !oPtr->has( LANGUAGES ) || !oPtr->has( LABELS ) ) {
    	throw Poco::Exception( Poco::format(
					"'strings.json' must consist of exactly two key-value's on base level: '%s' and '%s'",
					LANGUAGES, LABELS
		));
    }
    if( !oPtr->isArray( LANGUAGES ) ) {
    	throw Poco::Exception( Poco::format( "Value of key '%s' must be JSON array!", LANGUAGES ) );
    }
    if( !oPtr->isObject( LABELS ) ) {
    	throw Poco::Exception( Poco::format( "Value of key '%s' must be JSON array!", LABELS ) );
    }

    Poco::JSON::Array::Ptr languages = oPtr->getArray( LANGUAGES );
    Poco::JSON::Object::Ptr labels = oPtr->getObject( LABELS );
    std::size_t lSize = languages->size();
    if( lSize == 0 ) {
    	throw Poco::Exception( "There must be at least one language specified!" );
    }
    std::vector < std::string > langs;
    for( std::size_t i=0; i<lSize; i++ ) {
        langs.push_back( Poco::toUpper( languages->get( i ).toString() ) );
    }
    std::vector < std::map < std::string, std::string > > lStrs( lSize, std::map < std::string, std::string >() );
    for( Poco::JSON::Object::Iterator
            it = labels->begin(), itEnd = labels->end(); it != itEnd; it++ )
    {
        Poco::Dynamic::Var dValue = it->second;
        if( dValue.type() != typeid( Poco::JSON::Array::Ptr ) ) {
        	throw Poco::Exception( Poco::format( "Every key at '%s' must store JSON array!", LABELS ) );
        }
        Poco::JSON::Array::Ptr aPtr = dValue.extract < Poco::JSON::Array::Ptr >();
        if( aPtr->size() < lSize ) {
        	throw Poco::Exception( Poco::format(
        			"Every key at '%s' must store JSON array of size at least the same as '%s'",
					LABELS, LANGUAGES
			));
        }
        for( std::size_t i=0; i<lSize; i++ ) {
            lStrs.at( i )[ it->first ] = aPtr->get( i ).toString();
        }
    }

    for( std::size_t i=0; i<lSize; i++ ) {
    	mStrings[ langs.at( i ) ] = lStrs.at( i );
    }

    mCurLang = mStrings.begin()->first;
}

}
}
