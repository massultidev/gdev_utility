/* Copyright by: P.J. Grochowski */

#include "gdev/utility/Loggable.h"

#include <Poco/PatternFormatter.h>
#include <Poco/FormattingChannel.h>
#include <Poco/AsyncChannel.h>
#include <Poco/ConsoleChannel.h>
#include <Poco/FileChannel.h>
#include <Poco/Thread.h>
#include <Poco/Path.h>
#include <Poco/StringTokenizer.h>

namespace gdev {
namespace utility {

std::size_t Loggable::mLoggablesCounter = 0;
std::string Loggable::mOutputFormat = "[%Y-%m-%d][%H:%M:%S.%i][UTC]-[%s]-%q: %t";

void Loggable::init(
		Poco::Message::Priority priority,
		const std::string& path,
		const std::string& opt
)
{
	Poco::Logger::shutdown();

	Poco::PatternFormatter* patternFormatter = new Poco::PatternFormatter( mOutputFormat );

	Poco::Channel* channel = NULL;
	if( path.empty() ) {
		channel = new Poco::ConsoleChannel();
	} else {
		Poco::StringTokenizer st( opt, "|", Poco::StringTokenizer::TOK_IGNORE_EMPTY|Poco::StringTokenizer::TOK_TRIM );
		channel = new Poco::FileChannel( path );
		channel->setProperty( Poco::FileChannel::PROP_ROTATION, st[0] );
		channel->setProperty( Poco::FileChannel::PROP_ARCHIVE, st[1] );
		channel->setProperty( Poco::FileChannel::PROP_TIMES, st[2] );
		channel->setProperty( Poco::FileChannel::PROP_COMPRESS, st[3] );
		channel->setProperty( Poco::FileChannel::PROP_PURGECOUNT, st[4] );
	}

	Poco::FormattingChannel* fmtChannel = new Poco::FormattingChannel( patternFormatter, channel );
	Poco::AsyncChannel* asyncChannel = new Poco::AsyncChannel( fmtChannel, Poco::Thread::PRIO_LOWEST );
	asyncChannel->open();
	Poco::Logger::shutdown();
	Poco::Logger::root().setChannel( asyncChannel );
	Poco::Logger::root().setLevel( priority );
}

Loggable::Loggable( const std::string& name )
	: mName( name )
{
	mLoggablesCounter++;
}

Loggable::~Loggable() {
	mLoggablesCounter--;

	if( mLoggablesCounter == 0 ) {
		Poco::Logger::shutdown();
	}
}

Poco::Logger& Loggable::log() const {
	Poco::Logger* logger = Poco::Logger::has( mName );

	if( logger != NULL ) {
		return *logger;
	}

	try {
		return Poco::Logger::create(
				mName,
				Poco::Logger::root().getChannel(),
				Poco::Logger::root().getLevel()
		);
	} catch( ... ) {
		logger = Poco::Logger::has( mName );
		if( logger != NULL ) {
			return *logger;
		} else throw;
	}
}

}
}
